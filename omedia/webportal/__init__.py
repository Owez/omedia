from flask import Flask
from flask_bootstrap import Bootstrap
from omedia.utils import ConfigWebPortal

webportal_app = Flask(__name__)
Bootstrap(webportal_app)

webportal_config = ConfigWebPortal()

webportal_app.config["SECRET_KEY"] = webportal_config.SECRET_KEY

# NOTE Blueprint inits have to be below db init or
# the views cannot access the DB
from omedia.webportal.blueprints.misc import misc_blueprint

webportal_app.register_blueprint(misc_blueprint, url_prefix="/")
