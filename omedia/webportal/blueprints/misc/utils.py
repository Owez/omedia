from omedia.webportal.api_wrapper import OMediaApi

user_api_wrapper = OMediaApi().User()


def login_form(form):
    """
    Validates login form and tries to use data to login using the
    OMediaApi from omedia.utils

    - form: FlaskForm, the form used to check
    < N/A
    """

    if form.validate_on_submit():
        print("Login POST detected!")


def register_form(form):
    """
    Validates register form and tries to use data to register using the
    OMediaApi from omedia.utils

    - form: FlaskForm, the form used to check
    < N/A
    """

    if form.validate_on_submit():
        print("Register POST detected!")

        resp = user_api_wrapper.new_user(
            form.username.data, form.email.data, form.password.data
        )

        # TODO properly hook up requests
