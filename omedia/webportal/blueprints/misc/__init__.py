from flask import Blueprint, render_template
from omedia.webportal.blueprints.misc.forms import LoginForm, RegisterForm
from omedia.webportal.blueprints.misc.utils import login_form, register_form

misc_blueprint = Blueprint("misc", __name__, template_folder="templates/misc/")


@misc_blueprint.route("/", methods=["GET", "POST"])
def index():
    """
    Index page
    """

    usr_login_form = LoginForm()
    usr_register_form = RegisterForm()

    login_form(usr_login_form)  # Check login forms
    register_form(usr_register_form)  # Check register forms

    return render_template(
        "misc_index.html",
        usr_login_form=usr_login_form,
        usr_register_form=usr_register_form,
    )
