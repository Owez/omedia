import requests
from omedia.utils import ConfigApi

api_config = ConfigApi()


class OMediaApi:
    class User:
        def new_user(self, username, email, password):
            """
            Creates a new user from the api
            """

            return requests.post(
                api_config.API_ENDPOINT,
                data={
                    "username": username,
                    "email": email,
                    "password": password,
                },
            )
