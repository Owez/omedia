import os
import toml


class ConfigBase:
    """
    Basic need-to-know info for all mini-projects
    """

    CONFIG_PATH = f"config.toml"  # Where usually `config.toml` is kept
    CONFIG = toml.load(open(CONFIG_PATH, "r"))  # Serialized dict from toml
    SHOULD_DEBUG = CONFIG["should_debug"]


class ConfigApi:
    """
    Api endpoints to connect to and host from
    """

    config = ConfigBase().CONFIG

    API_DOMAIN = config["api"]["domain"]  # Example: 0.0.0.0
    API_PORT = config["api"]["port"]  # Example: 8080

    def __init__(self):
        self.SECRET_KEY = self._get_secret_key()
        self.API_ENDPOINT = "http://{0}:{1}/{2}".format(
            self.API_DOMAIN, self.API_PORT, self.config["api"]["endpoint"]
        )  # The api endpoint route

    def _get_secret_key(self):
        """
        Returns the secret key
        """

        return os.environ["API_SECRET_KEY"]


class ConfigWebPortal:
    """
    WebPortal endpoints to connect to and host from
    """

    config = ConfigBase().CONFIG

    API_DOMAIN = config["web_portal"]["domain"]  # Example: 0.0.0.0
    API_PORT = config["web_portal"]["port"]  # Example: 8080

    def __init__(self):
        self.SECRET_KEY = self._get_secret_key()

    def _get_secret_key(self):
        """
        Returns the secret key
        """

        return os.environ["WP_SECRET_KEY"]
