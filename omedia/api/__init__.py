from flask import Flask
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from omedia.utils import ConfigApi

api_app = Flask(__name__)
bcrypt = Bcrypt(api_app)

api_app.config["SECRET_KEY"] = ConfigApi().SECRET_KEY
api_app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///omediadb.sqlite3"
api_app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(api_app)

# NOTE Blueprint inits have to be below db init or
# the views cannot access the DB
from omedia.api.core import api_blueprint

api_app.register_blueprint(api_blueprint, url_prefix="/api/")
