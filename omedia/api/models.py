from omedia.api import db, bcrypt


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)
    username = db.Column(db.String)
    password_hash = db.Column(db.String)

    def __init__(self, username, email, password):
        """
        - username: Username of user
        - password_hash: The password of user (will be hashed)
        """

        self.username = username
        self.email = email
        self.password_hash = self._hash_password(password)

    def _hash_password(self, unhashed_password) -> str:
        """
        - unhashed_password: str, the unsecured password to hash
        < hashed password: str
        """

        return bcrypt.generate_password_hash(unhashed_password)

    def compare_passwords(self, unhashed_password) -> bool:
        """
        Compares an unsecured password to an existing one
        
        - unhashed_password: str, the unsecured password to compare
        < passwords match?: bool
        """

        return bcrypt.check_password_hash(
            self.password_hash, unhashed_password
        )
