from flask import Blueprint
from flask_restful import Api, Resource
from flask_restful.reqparse import RequestParser
from omedia.api import db
from omedia.api.models import User

api_blueprint = Blueprint("api", __name__)
api = Api(api_blueprint, prefix="/")

user_make = RequestParser(bundle_errors=True)

# user_make.add_argument("username", type=str, required=True)
# user_make.add_argument("email", type=str, required=True)
# user_make.add_argument("password", type=str, required=True)
user_make.add_argument("username", type=str, required=False)
user_make.add_argument("email", type=str, required=False)
user_make.add_argument("password", type=str, required=False)


class UserMake(Resource):
    def post(self):
        args = user_make.parse_args()

        new_user = User(
            username=args["username"],
            email=args["email"],
            password=args["password"],
        )

        db.session.add(new_user)
        db.session.commit()

        return (
            {
                "head": {
                    "status_code": 200,
                    "desc": "New user was added sucsessfully!",
                },
                "body": {"inputted_data": args},
            },
            200,
        )


api.add_resource(UserMake, "/")
