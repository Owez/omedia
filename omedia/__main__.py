import click
from omedia.api import db, api_app
from omedia.webportal import webportal_app
from omedia.utils import ConfigBase, ConfigApi, ConfigWebPortal


@click.group()
def base_group():
    pass


@click.command()
def webportal():
    """
    Runs the webportal mini-project (like the discord bot but on a website ui)
    """

    config_webportal = ConfigWebPortal()

    webportal_app.run(
        host=config_webportal.API_DOMAIN,
        port=config_webportal.API_PORT,
        debug=ConfigBase().SHOULD_DEBUG,
    )


@click.command()
def api():
    """
    The core API of this project, RESTFUL and built with flask & flask-restful
    """

    config_api = ConfigApi()

    api_app.run(
        host=config_api.API_DOMAIN,
        port=config_api.API_PORT,
        debug=ConfigBase().SHOULD_DEBUG,
    )


@click.command()
def make_api_db():
    """
    Generates a blank database for the api
    """

    db.create_all()


base_group.add_command(api)
base_group.add_command(make_api_db)
base_group.add_command(webportal)

if __name__ == "__main__":
    base_group()
